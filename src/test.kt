fun main(args: Array<String>) {
    val v_1_1 = 'e'
    if (v_1_1 in 'a' .. 'z' step 1) {
        println(v_1_1)
        for (i_1_2 in 31 .. 128 step 1) {
            println(i_1_2)
            for (i_1_3 in 24L downTo -73L step 1) {
                println(i_1_3)
            }
        }
    }
    val v_2_1 = 'e'
    if (v_2_1 in 'a' until 'z' step 1) {
        println(v_2_1)
        for (i_1_2 in 64 until 101 step 1) {
            println(i_1_2)
            for (i_1_3 in 'a' .. 'z' step 1) {
                println(i_1_3)
            }
            for (i_2_3 in 44 until 98 step 1) {
                println(i_2_3)
            }
        }
        val list_2_2 = 48 downTo -19 step 1
        for (i_2_2 in list_2_2) {
            println(i_2_2)
            val v_1_3 = 81
            if (v_1_3 in 77 until 107 step 1) {
                println(v_1_3)
            }
            val tmp_2_3 = listOf(1, 2, 3, 4, 5)
            val list_2_3 = tmp_2_3.indices
            val v_2_3 = -2
            if (v_2_3 in list_2_3) {
                println(v_2_3)
            }
        }
    }
}
