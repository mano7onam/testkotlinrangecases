fun main(args: Array<String>) {
	var sum = 0
	var rng1 = 0 .. 100
	var rng2 = 100 downTo 0 step 2
	var rng3 = 0 until 100000
	
	for (i in rng1) {
		for (j in rng2) {
			for (k in rng3) {
				sum += i + j + k
			}
		}
	}
	
	println(sum)
}
