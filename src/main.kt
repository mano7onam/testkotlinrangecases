import java.io.PrintWriter
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

class TestGenerator (pFileName : String) {
    private val fileName = pFileName
    val fileWriter = PrintWriter(fileName)
    private val maxDepth = 3
    private val maxStep = 3

    private val defaultMaxStart = 100
    private val defaultMaxInterval = 100
    private val defaultMaxRangeStep = 10
    private val defaultIntForRand = 100
    private val defaultNumberOfExpressions = 100

    private val rand = Random()

    private fun printlnWithTabs(depth: Int, line : String) {
        for (i in 0 until depth) {
            fileWriter.print("  ")
        }
        fileWriter.println(line)
    }

    enum class RangeType {
        RANGETO, DOWNTO, UNTIL
    }

    enum class RangeValuesType {
        INT, LONG, CHAR
    }

    enum class RangeBoundType {
        CONST, VARIABLE, FUNCTION
    }

    open class RangeBound (val rangeValueType : RangeValuesType,
                           val type : RangeBoundType,
                           val name : String,
                           val value : Long)
    {
        override fun toString() : String {
            return name
        }

        operator fun compareTo(other : CharRangeBound) : Int {
            return when {
                value < other.value -> -1
                value > other.value -> 1
                else -> 0
            }
        }
    }

    class CharRangeBound (rangeValueType : RangeValuesType,
                          type : RangeBoundType,
                          name : String,
                          value : Long) : RangeBound(rangeValueType, type, name, value)
    {
        constructor(value : Char) :
                this(RangeValuesType.CHAR, RangeBoundType.CONST, "none", value.toLong())

        override fun toString() : String {
            if (type == RangeBoundType.CONST) {
                return "'" + value.toString() + "'"
            } else {
                return super.toString()
            }
        }
    }

    class IntRangeBound (rangeValueType : RangeValuesType,
                         type : RangeBoundType,
                         name : String,
                         value : Long) : RangeBound(rangeValueType, type, name, value)
    {
        constructor(value : Int) :
                this(RangeValuesType.CHAR, RangeBoundType.CONST, "none", value.toLong())

        override fun toString() : String {
            if (type == RangeBoundType.CONST) {
                return value.toString()
            } else {
                return super.toString()
            }
        }
    }

    class LongRangeBound (rangeValueType : RangeValuesType,
                          type : RangeBoundType,
                          name : String,
                          value : Long) : RangeBound(rangeValueType, type, name, value)
    {
        constructor(value : Long) :
                this(RangeValuesType.CHAR, RangeBoundType.CONST, "none", value)

        override fun toString() : String {
            if (type == RangeBoundType.CONST) {
                return value.toString() + "L"
            } else {
                return super.toString()
            }
        }
    }

    val charExpressions : ArrayList<CharRangeBound> = ArrayList()
    val intExpressions : ArrayList<IntRangeBound> = ArrayList()
    val longExpressions : ArrayList<LongRangeBound> = ArrayList()

    open class RangeInfo (
            val rangeType : RangeType,
            val a : RangeBound,
            val b : RangeBound,
            val step : Int = 1)
    {

        private fun rangeTypeToString(rangeType : RangeType) : String {
            return when (rangeType) {
                RangeType.RANGETO -> ".."
                RangeType.DOWNTO -> "downTo"
                else -> "until"
            }
        }

        override fun toString() : String {
            if (step != 1) {
                return "$a ${rangeTypeToString(rangeType)} $b} step $step"
            } else {
                return "$a ${rangeTypeToString(rangeType)} $b}"
            }
        }
    }

    private fun getRange(arr : ArrayList<*>) : RangeInfo {
        var indA = rand.nextInt(arr.size)
        var indB = rand.nextInt(arr.size)
        if (indA > indB) {
            indA = indB .also {indB = indA}
        }
        val r = rand.nextInt(3)
        val rangeStep = rand.nextInt(defaultMaxRangeStep) + 1
        val paramA = arr[indA] as RangeBound
        val paramB = arr[indB] as RangeBound
        return when (r) {
            0 -> RangeInfo(RangeType.DOWNTO, paramB, paramA, rangeStep + 1)
            1 -> RangeInfo(RangeType.RANGETO, paramA, paramB, rangeStep + 1)
            2 -> RangeInfo(RangeType.UNTIL, paramA, paramB, rangeStep + 1)
            else -> RangeInfo(RangeType.RANGETO, paramA, paramB, rangeStep + 1)
        }
    }

    private fun getRange() : RangeInfo {
        val r = rand.nextInt(3)
        return when (r) {
            0 -> getRange(intExpressions)
            1 -> getRange(charExpressions)
            else -> getRange(longExpressions)
        }
    }

    private fun genForFromConsts(step: Int, depth : Int) {
        val forCounterName = "i_${step}_$depth"
        val rangeInfo = getRange()
        printlnWithTabs(depth, "for ($forCounterName in $rangeInfo) {")
        printlnWithTabs(depth + 1, "println($forCounterName)")
    }

    private fun genForFromNewList(step : Int, depth : Int) {
        val forCounterName = "i_${step}_$depth"
        val tmpName = "tmp_${step}_$depth"
        val listName = "list_${step}_$depth"
        val r = rand.nextInt(2)
        when (r) {
            0 -> {
                printlnWithTabs(depth, "val $tmpName = listOf(1, 2, 3, 4, 5)")
                printlnWithTabs(depth, "val $listName = $tmpName.indices")
            }
            1 -> {
                val rangeInfo = getRange()
                printlnWithTabs(depth, "val $listName = $rangeInfo")
            }
        }
        printlnWithTabs(depth, "for ($forCounterName in $listName) {")
        printlnWithTabs(depth + 1, "println($forCounterName)")
    }

    private fun genDefinitionIfVar(ifVarName : String, rangeInfo : RangeInfo, depth : Int) {
        if (rangeInfo.rangeType == RangeType.DOWNTO) {
            when (rangeInfo.a.rangeValueType) {
                RangeValuesType.INT -> printlnWithTabs(depth, "val $ifVarName = ${rangeInfo.a.value - 4}")
                RangeValuesType.LONG -> printlnWithTabs(depth, "val $ifVarName = ${rangeInfo.a.value - 4L}L")
                RangeValuesType.CHAR -> printlnWithTabs(depth, "val $ifVarName = '${(rangeInfo.a.value - 4).toChar()}'")
            }
        } else {
            when (rangeInfo.a.rangeValueType) {
                RangeValuesType.INT -> printlnWithTabs(depth, "val $ifVarName = ${rangeInfo.a.value + 4}")
                RangeValuesType.LONG -> printlnWithTabs(depth, "val $ifVarName = ${rangeInfo.a.value + 4L}L")
                RangeValuesType.CHAR -> printlnWithTabs(depth, "val $ifVarName = '${(rangeInfo.a.value + 4).toChar()}'")
            }
        }
    }

    private fun genIfFromNewList(step : Int, depth : Int) {
        val ifVarName = "v_${step}_$depth"
        val tmpName = "tmp_${step}_$depth"
        val listName = "list_${step}_$depth"
        val r = rand.nextInt(2)
        when (r) {
            0 -> {
                printlnWithTabs(depth, "val $tmpName = listOf(1, 2, 3, 4, 5)")
                printlnWithTabs(depth, "val $listName = $tmpName.indices")
                val rr = rand.nextInt(8) - 2
                printlnWithTabs(depth, "val $ifVarName = $rr")
            }
            1 -> {
                val rangeInfo = getRange()
                genDefinitionIfVar(ifVarName, rangeInfo, depth)
                printlnWithTabs(depth, "val $listName = $rangeInfo")
            }
        }
        printlnWithTabs(depth, "if ($ifVarName in $listName) {")
        printlnWithTabs(depth + 1, "println($ifVarName)")
    }

    private fun genIfFromConsts(step : Int, depth : Int) {
        val rangeInfo = getRange()
        val ifVarName = "v_${step}_$depth"

        genDefinitionIfVar(ifVarName, rangeInfo, depth)

        printlnWithTabs(depth, "if ($ifVarName in $rangeInfo) {")
        printlnWithTabs(depth + 1, "println($ifVarName)")
    }

    private fun generateBody(depth: Int) {
        val steps = rand.nextInt(maxStep) + 1
        for (i in 1 .. steps) {
            val type = rand.nextInt(4)
            when (type) {
                0 -> genForFromConsts(i, depth)
                1 -> genForFromNewList(i, depth)
                2 -> genIfFromNewList(i, depth)
                3 -> genIfFromConsts(i, depth)
            }
            if (depth < maxDepth) {
                generateBody(depth + 1)
            }
            printlnWithTabs(depth, "}")
        }
    }

    private fun emitCharFunction(depth : Int, id : Int, value : Char) : String {
        val name = "charFun$id"
        printlnWithTabs(depth, "fun $name() = '$value'")
        return name
    }

    private fun emitCharVariable(depth : Int, id : Int, value : Char) : String {
        val name = "charVar$id"
        printlnWithTabs(depth, "$name = '$value'")
        return name
    }

    private fun emitIntFunction(depth : Int, id : Int, value : Int) : String {
        val name = "intFun$id"
        printlnWithTabs(depth, "fun $name() = $value")
        return name
    }

    private fun emitIntVariable(depth : Int, id : Int, value : Int) : String {
        val name = "intVar$id"
        printlnWithTabs(depth, "$name = $value")
        return name
    }

    private fun emitLongFunction(depth : Int, id : Int, value : Long) : String {
        val name = "longFun$id"
        printlnWithTabs(depth, "fun $name() = ${value}L")
        return name
    }

    private fun emitLongVariable(depth : Int, id : Int, value : Long) : String {
        val name = "longVar$id"
        printlnWithTabs(depth, "$name = ${value}L")
        return name
    }

    private fun generateExpressions(depth : Int) {
        for (i in 0 until defaultNumberOfExpressions) {
            val c = 'a' + rand.nextInt(25)
            val r = rand.nextInt(3)
            when (r) {
                0 -> charExpressions.add(CharRangeBound(c))
                1 -> {
                    val name = emitCharFunction(depth, i, c)
                    charExpressions.add(CharRangeBound(
                            RangeValuesType.CHAR, RangeBoundType.FUNCTION, name, c.toLong()))
                }
                2 -> {
                    val name = emitCharVariable(depth, i, c)
                    charExpressions.add(CharRangeBound(
                            RangeValuesType.CHAR, RangeBoundType.VARIABLE, name, c.toLong()))
                }
            }
        }
        for (i in 0 until defaultNumberOfExpressions) {
            val value = rand.nextInt(defaultIntForRand) - rand.nextInt(defaultIntForRand)
            val r = rand.nextInt(3)
            when (r) {
                0 -> intExpressions.add(IntRangeBound(value))
                1 -> {
                    val name = emitIntFunction(depth, i, value)
                    intExpressions.add(IntRangeBound(
                            RangeValuesType.INT, RangeBoundType.FUNCTION, name, value.toLong()))
                }
                2 -> {
                    val name = emitIntVariable(depth, i, value)
                    intExpressions.add(IntRangeBound(
                            RangeValuesType.INT, RangeBoundType.VARIABLE, name, value.toLong()))
                }
            }
        }
        for (i in 0 until defaultNumberOfExpressions) {
            val value = (rand.nextInt(defaultIntForRand) - rand.nextInt(defaultIntForRand)).toLong()
            val r = rand.nextInt(3)
            when (r) {
                0 -> longExpressions.add(LongRangeBound(value))
                1 -> {
                    val name = emitLongFunction(depth, i, value)
                    longExpressions.add(LongRangeBound(
                            RangeValuesType.LONG, RangeBoundType.FUNCTION, name, value))
                }
                2 -> {
                    val name = emitLongVariable(depth, i, value)
                    longExpressions.add(LongRangeBound(
                            RangeValuesType.LONG, RangeBoundType.VARIABLE, name, value))
                }
            }
        }
    }

    fun generate() {
        printlnWithTabs(0, "fun main(args: Array<String>) {")
        generateExpressions(1)
        println("generated expressions")
        generateBody(1)
        println("generated body")
        printlnWithTabs(0, "}")
        fileWriter.close()
    }
}

fun main(args: Array<String>) {
    for (i in 0 .. 0) {
        println(i)
        val testGenerator = TestGenerator("input.kt")
        testGenerator.generate()
        val processCompile = ProcessBuilder("/home/mano/KOTLIN/kotlin-native/dist/bin/konanc", "input.kt").start()
        processCompile.waitFor()
        println("Compiled")
        val logPath = Paths.get("/home/mano/KOTLIN/ForGenerator")
        val processRun1 = ProcessBuilder("/home/mano/KOTLIN/ForGenerator/program.kexe")
        processRun1.redirectOutput(ProcessBuilder.Redirect.to(logPath.resolve("output1.txt").toFile()))
        processRun1.start().waitFor()
        println("Runned 1")
        val processRun2 = ProcessBuilder("/home/mano/KOTLIN/ForGenerator/program.kexe")
        processRun2.redirectOutput(ProcessBuilder.Redirect.to(logPath.resolve("output2.txt").toFile()))
        processRun2.start().waitFor()
        println("Runned 2")
        val f1 = Files.readAllBytes(Paths.get("/home/mano/KOTLIN/ForGenerator/output1.txt"))
        val f2 = Files.readAllBytes(Paths.get("/home/mano/KOTLIN/ForGenerator/output2.txt"))
        var equals = f1.size == f2.size
        if (!equals) {
            println("FAILED")
            break
        }
        for (j in 0 until f1.size) {
            if (f1[j] != f2[j]) {
                equals = false
                break
            }
        }
        if (equals) {
            println("OK")
        } else {
            println("FAILED")
            break
        }
    }
}